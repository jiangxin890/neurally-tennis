# Neutrally Tennis



## Project Overview
The project aims to provide a platform for players to get a neutral point that shows the ratio of win and lose to gain a UTR score so that players can protect their UTR by winning the margin.

Produce an Android app calculating the neutral point.

## Project Purpose
The UTR ranking system will consider two components’ current UTR score and the number of winning games in a match to determine whether to increase the UTR score for the players or not. Many school tennis players are struggling to figure out whether they can increase their UTR score in the match. Parents are also very stressed because simply winning a match can not guarantee their children to increase their UTR score. In this project, the application we developed will help tennis players to figure out the neutral point of gaining UTR score for the tennis players by entering their UTR score and their components’ UTR score.

## Team Chart
| Name        | UID      | Role                |
|-------------|----------|---------------------|
| Ric Curnow  | -        | Client              |
| Jizhou Ren  | u7270477 | Spokesperson/Member |
| Kehui Xu    | u6204275 | Spokesperson/Member |
| Ziwei Cheng | u6915666 | Member              |
| Xiaoyu Liu  | u7352216 | Member              |
| Xinyi Jiang | u6891814 | Member              |
| Yuehao Zhai | u7374554 | Member              |

## Task Management
- [Decision Log](https://docs.google.com/spreadsheets/d/1pD_TU-HmK2Tyl5OhU-95JJMaJ9FZ3dTjvW8gC9zSeNs/edit#gid=938256009)
- [Reflection Log](https://docs.google.com/spreadsheets/d/1pD_TU-HmK2Tyl5OhU-95JJMaJ9FZ3dTjvW8gC9zSeNs/edit#gid=2526034)
- [Workload track](https://docs.google.com/spreadsheets/d/1pD_TU-HmK2Tyl5OhU-95JJMaJ9FZ3dTjvW8gC9zSeNs/edit#gid=1718860233)

## Weekly Newsletter
- [W3 Newsletter](https://docs.google.com/document/d/1hRqTBQTqPqnHG6VVxJ7QnFPcmyTtHFNosOHRzMTyywY/edit?usp=sharing)
- [W4 Newsletter](https://docs.google.com/document/d/1gbk0GuifM3EEY8buHsE8BczT-TLHIUckZbQKxW773LE/edit?usp=sharing)
- [W5 Newsletter](https://docs.google.com/document/d/1XjAoUdmlAKZdm58N9wlSl_0whuEedIj2dfLcPwzcsMU/edit?usp=sharing)
- [W7 Newsletter](https://docs.google.com/document/d/1FguEAFCCg10-SoilFlGjhRgNIF1m3JOn/edit?usp=sharing&ouid=110732081530349838357&rtpof=true&sd=true)
- [W9 Newsletter](https://docs.google.com/document/d/1T9yDsLsPT-yroa5pxxeW0WtcJ2jvYnxp/edit?usp=sharing&ouid=110732081530349838357&rtpof=true&sd=true)


##  Audit 1
- [Audit 1 slides](https://docs.google.com/presentation/d/1glYLJwvtd3tYuOTMFQNvZ1Jgl78EWmwaXUoAkpHF-D0/edit?usp=sharing)
- [Statement of Work](https://docs.google.com/document/d/1YEsu1Oa78VhTgeoh7UxZn-eDMUwt2VYA/edit)

- [Technical Constraints](https://docs.google.com/document/d/1SkBuUk8DTDknGzNYg4xWzs63InnMj13RDvWABOTmk_g/edit)

- [Resources and Risks](https://docs.google.com/document/d/1rrPvBOnpyZPB519S9HXYSzzTyN2wkB966CG-c5JWUJA/edit)
- [Project Client Map](https://docs.google.com/document/d/10euLkBKw0Re1hDAxCy788yrAzIWQlBwJkkUWLlMfASI/edit)
- [Objectives](https://docs.google.com/document/d/1mzRiUCt2YXTO2SB9_WkIBkvGFgwgFWF50bsx_y309JM/edit)
- [Stakeholder Analysis](https://docs.google.com/document/d/16p9Iv2fi7hbTZ0_tmIXtThmlEhlAPLC7GJz5rQ65g_A/edit)

##  Audit 2
- [Audit 2 Slides](https://docs.google.com/presentation/d/1ljS_so77JSMczx-jLwRvYpUJJ5BdMoNAUu1ghyn5xgI/edit?usp=sharing)
- [Acceptance Criteria](https://docs.google.com/document/d/1OKYWvxthu_uocf_P1Qh_3QxJM_HPurdA/edit?usp=drive_web&ouid=115015180231522689296&rtpof=true)
- [Output for App](https://docs.google.com/document/d/11m2CoFEM213gyl_Vz9lAVyvtzI27ors5/edit?usp=drive_web&ouid=115015180231522689296&rtpof=true)
- [Output for Algorithm](https://gitlab.cecs.anu.edu.au/u7270477/neutrally-tennis-repository/-/blob/main/Algorithm/tennis.ipynb)
- [Dataset](https://docs.google.com/spreadsheets/d/1PWAutVqRbIYEqDMS-b-VIhH0hdyaS54FOUAI9px3mcc/edit?usp=sharing)
##  Audit 3

- [Audit 3 Slides](https://docs.google.com/presentation/d/17-fh-_SOz6E6cJfxmKop2fCZ1ivl0y2kPDtzOISJioY/edit?usp=sharing)
- [Total Dataset](https://docs.google.com/spreadsheets/d/1NxCWxDHdvkKLEUf_j3-xi2MAUKw6TFuM056qxcfzBVU/edit#gid=1961632993)
- [Cleaned Dataset](https://docs.google.com/spreadsheets/d/1aKMP60BDhjhoz2Aivx5JeZmPUJChDw1j-LYSm-Dg9oE/edit#gid=1795336766)
- [Trello](https://trello.com/b/5LL22Tnw/neutrally-tennis)
- [Output for App](https://docs.google.com/document/d/15DAeBZThPng7vYf06t5wT96ZofdC-FI3/edit?usp=sharing&ouid=110732081530349838357&rtpof=true&sd=true)
- [Output for Algorithm](https://docs.google.com/document/d/1rGYhiV6iDYCexIqDBY2wo-BhpZYsnhHUs4qADK0zDJc/edit)
- [Output for Website](https://docs.google.com/document/d/1h1yBCTsiKc1KrdLYiYd_qLq6aIcZk9btXO1J8XDEZ1k/edit)
- [Security Part](https://docs.google.com/document/d/1BgnoN-XuUdlsDi2n6j3g0JER2ILRUDhF/edit?usp=drive_web&ouid=115015180231522689296&rtpof=true)
##  Meeting Minutes
### Tutorial Meeting
- [Week2 Tutorial Meeting](https://docs.google.com/document/d/1D_QPH6T4MB8Vz4E15KTJiBaw6vl-iPwl/edit)
- [Week3 Tutorial Meeting](https://docs.google.com/document/d/10U-oVZ8_0bfiXV0nGt0DXUr_VFQFOaht/edit?usp=sharing&ouid=110732081530349838357&rtpof=true&sd=true)
- [Week4 Tutorial Meeting](https://docs.google.com/document/d/1uGrHg2HAgstvYXIqBvGsgzlruuJg5CUqN-LHaO2JXaA/edit)
- [Week5 Tutorial Meeting](https://docs.google.com/document/d/1a4HGIlj-z8Aa3YfQOZb--ESA6XdnDoXLNkrnpEdB5gI/edit)
- [Week7 Tutorial Meeting](https://docs.google.com/document/d/1querBHDs8hhEiFkTQTl2qGIfmNF7CZH685512lUr9gY/edit?usp=sharing)
- [Week9 Tutorial Meeting](https://docs.google.com/document/d/1EZ5_5wLUdHshtHF-cbYNsLof6i8cIXXtWe0zCoksSJ0/edit?usp=sharing)
### Team Meeting
- [Week2 Team Meeting 1](https://docs.google.com/document/d/1ykEqtrFY5zEXmXDfHA6lxqDUFZB85SIG3-HOAKy6WoY/edit)
- [Week2 Team Meeting 2](https://docs.google.com/document/d/13Q63iemKWvzW9T-SdErFgCZIb6Db8FcNdpN5M1n-tHE/edit)
- [Week2 Team Meeting 3](https://docs.google.com/document/d/1HxmOSsKkuisMTwU_o-sVhBZy1p-q-Rw0vdMUtIKKjTc/edit)
- [Week4 Team Meeting 4](https://docs.google.com/document/d/1hBXUqkNFBQR9qh-lhhpAUr2p-Leh_Wqg2VQP6EVUVpE/edit?usp=sharing)
- [Week4 Team Meeting 5](https://docs.google.com/document/d/1uB-PC1xE0s7tq1pvLYBXu48AO2-PTYA03RV53OosRHU/edit?usp=sharing)
- [Week5 Team Meeting 6](https://docs.google.com/document/d/1zrBQ6srayMnSPEA9fT-Lms2ZGW1iLeB9-IZPBwGv7HI/edit?usp=sharing)
- [Week5 Team Meeting 7](https://docs.google.com/document/d/1EtxPK2CUnnL1jSIV0RnSu-7f63eCqaAbEZW4C56TK68/edit?usp=sharing)
- [Week5 Team Meeting 8](https://docs.google.com/document/d/1osLdpZan94yxfsxyGS1NM0XbGmZy7htxv7XW9yrjMlo/edit?usp=sharing)
- [Week6 Team Meeting 9](https://docs.google.com/document/d/1HE6FtKGT3PdJqI9aa9XSkWeVbzIEos09086zbL15kLY/edit?usp=sharing)
- [Mid-break Meeting 10](https://docs.google.com/document/d/1o-YvL-CVvQuQZv-ycz158ULqRAisEyXO19F-v_qHtYs/edit)
- [Mid-break Meeting 11](https://docs.google.com/document/d/13tnzPR9Mzk780cSOmngYlgWqBYNQElAoAXAhwsYuMOk/edit)
- [Week7 Team Meeting 12](https://docs.google.com/document/d/1ALBTjuOUbqEv-CCnbOLSpe4vl1tR2HkqmwQ7UXhUzJQ/edit?usp=sharing)
- [Week8 Team Meeting 13](https://docs.google.com/document/d/1AdX9QbaR01qBlANuVV1daw0jU3KwshX_UB9xBqFo0ss/edit?usp=sharing)
- [Week9 Team Meeting 14](https://docs.google.com/document/d/1OFW5z0Iy7dcK4OlWhgB2gkclPoS5fMvLAJe6M8ReNvg/edit?usp=sharing)
- [Week9 Team Meeting 15](https://docs.google.com/document/d/1cBaWdGIemgyL82VYBpaEkC0cUVLAmHjR8_YBA2c9n1c/edit?usp=sharing)

### Client Meeting
- [Week1 Client Meeting 1](https://docs.google.com/document/d/1mJIzJIxi6xnEyqEDVI-ill7Lswg8SPQQERAEL0-mJ2g/edit)
- [Week3 Client Meeting 2](https://docs.google.com/document/d/1R5WcQJnwfn_kA8rtHLQkmsNoLc5A1GEXxh_W5POX1Cw/edit)
- [Week4 Client Meeting 3](https://docs.google.com/document/d/12g2NzG2rCb5boTiZVoAg8BP0Db1p_PfaMWRbXpoHtN0/edit?usp=sharing)
- [Week5 Client Meeting 4](https://docs.google.com/document/d/19__K-LeSZM5e8nJr6iO1XRLRvBAA80VVpeznbzovtGg/edit?usp=sharing)
- [Break Client Meeting 5](https://docs.google.com/document/d/1KVBR1-YwbFmh5rULS7vh-o8I5mNoLrc6toK0lvH9zkE/edit?usp=sharing)
- [Week7 Client Meeting 6](https://docs.google.com/document/d/1xz6uxi95PQ0NdlWEKQOUE1i0mF93OLYa4mKbwVZ7SyE/edit?usp=sharing)
- [Week9 Client Meeting 7](https://docs.google.com/document/d/1Oan5jZwgBuLlA1PTlp47a4Zd5jN69rTwod7zLgTqsUg/edit?usp=sharing)

##  Milestone
![Milestone](./milestone.png)


##  Repository
- [Algorithm](https://gitlab.cecs.anu.edu.au/u7270477/neutrally-tennis-repository/-/tree/main/Algorithm)
- [Application (Andriod Studio)](https://github.com/Kehui-xu/NeutrallyTennis)
- [Application (Android Studio) Manager function](https://github.com/YuehaoZhai/Neutrally-Tennis-Manager-)
- [Web](https://gitlab.cecs.anu.edu.au/u7270477/neutrally-tennis-repository/-/tree/main/Web/Neutrally-tennis)

